package helper

/*
	GROUP 1
	Contains Function
	Pham Tan Loc
*/

import (
	"reflect"
	"strings"
)

// Contains function
// So sanh chuoi/so voi chuoi/Array/Slice
func Contains(element interface{}, parent interface{}) (finalReturn bool) {
	defer func() {
		if err := recover(); err != nil {
			finalReturn = false
		}
	}()
	// 1: luon luon lay Value
	parentValue := reflect.ValueOf(parent)
	elementValue := reflect.ValueOf(element)

	// 2: kiem tra Kind la Slice hoac Array hoac String
	if parentValue.Kind() != reflect.Slice && parentValue.Kind() != reflect.Array && parentValue.Kind() != reflect.String {
		panic("Invalid Parent Kind")
	}
	if elementValue.Kind() == reflect.Slice || elementValue.Kind() == reflect.Array || elementValue.Kind() == reflect.Slice || elementValue.Kind() == reflect.Struct {
		panic("Invalid Element Kind")
	}
	// Xu ly neu parent la chuoi
	if parentValue.Kind() == reflect.String {
		if elementValue.Kind() != reflect.String {
			return false
		}
		va1 := elementValue.Interface().(string)
		va2 := parentValue.Interface().(string)
		if strings.Index(va2, va1) > -1 {
			return true
		}
	}
	for i := 0; i < parentValue.Len(); i++ {
		va1 := elementValue.Interface()
		va2 := parentValue.Index(i).Interface()
		if va1 == va2 {
			return true
		}
	}
	return false
}
