package helper

/*
	GROUP 1
	Testing Contains Function
	Pham Tan Loc
*/

import "testing"

// Test string (not) contains string
func TestContainsString2String(t *testing.T) {
	type args struct {
		element interface{}
		parent  interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test string contains string",
			args: args{
				element: "Go",
				parent:  "Learning Golang Thang",
			},
			want: true,
		},
		{
			name: "Test string not contains string",
			args: args{
				element: "Go1",
				parent:  "Learning Golang Thang",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.element, tt.args.parent); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Test string (not) contains array string
func TestContainsString2ArrayString(t *testing.T) {
	type args struct {
		element interface{}
		parent  interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test string contains array string",
			args: args{
				element: "Go",
				parent:  []string{"Go", "lang", "thang"},
			},
			want: true,
		},
		{
			name: "Test string not contains array string",
			args: args{
				element: "Go1",
				parent:  []string{"Go", "lang", "thang"},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.element, tt.args.parent); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Test Int32 (not) contains array Int32
func TestContainsInt2ArrayInt(t *testing.T) {
	type args struct {
		element interface{}
		parent  interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test integer contains array integer",
			args: args{
				element: int32(123), //ep kieu moi so sanh dung
				parent:  []int32{1, 234, 123},
			},
			want: true,
		},
		{
			name: "Test integer not contains array integer",
			args: args{
				element: int32(12345),
				parent:  []int32{1, 234, 123},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.element, tt.args.parent); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Test float64 (not) contains array float64
func TestContainsFloat2ArrayFloat(t *testing.T) {
	type args struct {
		element interface{}
		parent  interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test float contains array float",
			args: args{
				element: float64(123), //ep kieu moi so sanh dung
				parent:  []float64{1, 234, 123, 456, 789},
			},
			want: true,
		},
		{
			name: "Test float not contains array float",
			args: args{
				element: float64(12345),
				parent:  []float64{1, 234, 123, 456, 789},
			},
			want: false,
		},
		{
			name: "Test float not contains array float (empty)",
			args: args{
				element: float64(12345),
				parent:  []float64{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.element, tt.args.parent); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Test exception
func TestContainsException(t *testing.T) {
	type args struct {
		element interface{}
		parent  interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test raise exception",
			args: args{
				element: []int{1, 2, 3},
				parent:  "Hello Golang Thang",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.element, tt.args.parent); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}
