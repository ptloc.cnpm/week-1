package helper

import "reflect"

// IsEmpty Function
func IsEmpty(obj interface{}) bool {
	value := reflect.ValueOf(obj)
	if !(value.Kind() == reflect.Slice || value.Kind() == reflect.Array || value.Kind() == reflect.Struct) {
		if obj == nil || obj == "" || obj == false || obj == 0 {
			return true
		}
		return false
	}
	if value.Kind() == reflect.Slice || value.Kind() == reflect.Array {
		if value.Len() == 0 {
			return true
		}
		return false
	}
	if value.Kind() == reflect.Struct {
		for i := 0; i < value.NumField(); i++ {
			field := value.Field(i)
			valueOfField := field.Interface()
			if !(valueOfField == nil || valueOfField == "" || valueOfField == false || valueOfField == 0) {
				return false
			}
		}
		return true
	}
	return false
}
