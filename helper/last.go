package helper

import "reflect"

func Last(obj interface{}) interface{} {
	value := reflect.ValueOf(obj)
	if value.Kind() == reflect.Slice || value.Kind() == reflect.Array {
		if value.Len() == 0 {
			return false
		}
		l := value.Len()
		return value.Index(l - 1).Interface()
	}
	return false
}
