package helper

import (
	"reflect"
	"testing"
)

func TestLast(t *testing.T) {
	a := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	b := [0]int{}
	c := [3]int{2, 5, 6}
	d := 10
	type args struct {
		obj interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			name: "Case 1",
			args: args{
				obj: a,
			},
			want: 10,
		},
		{
			name: "Case 2",
			args: args{
				obj: b,
			},
			want: false,
		},
		{
			name: "Case 3",
			args: args{
				obj: c,
			},
			want: 5,
		},
		{
			name: "Case 4",
			args: args{
				obj: d,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Last(tt.args.obj); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Last() = %v, want %v", got, tt.want)
			}
		})
	}
}
